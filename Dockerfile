FROM ubuntu:xenial

RUN dpkg --add-architecture i386 && \
apt-get update && apt-get -y install build-essential vim-common wget git bzip2 make python libc6:i386 astyle clang cmake cppcheck && \
wget https://developer.arm.com/-/media/Files/downloads/gnu-rm/6-2017q2/gcc-arm-none-eabi-6-2017-q2-update-linux.tar.bz2&& \
mkdir -p /usr/local/bin/ && \
tar -xf gcc-arm-none-eabi-6-2017-q2-update-linux.tar.bz2 -C /usr/local/bin/
